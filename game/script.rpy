﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define cap = Character("Captain")
define gm = Character("Game Master")
define bar = Character("Bartender")


# The game starts here.

label start:

    # Scene clears the stuff on screen and displays a new background
    # Show will replace the back ground or show an image on top of the background
    # can use show example happy at right // example
    # for dialogue use "player" "Hey I talk!" to write dialogue
    # you can also use a define character such as the example     e "You've created a new Ren'Py game."

    # These display lines of dialogue.

    scene bg start

    cap "People have been going missing for years in this town. No one really questioned it, thinking it was a crime element or simply accidents. But as the militia captain you know better. You haven’t had any problems with the locals."

    cap "Most towns have some element of organised crime, muggings, murders, arson. But even the best have gamblers, beggars and other rabble, but not this town."

    "People went missing and didn't turn up at all, dead or otherwise. It's as if one day they just left. Then one day your son went missing, it became personal. Since your wife left, you’ve been raising him alone, luck be had it your position has a comfortable pay. "

    "He was, no he IS growing up to be quite a good strong lad. He is a good hunter, skilled with bait and trapping, and fishing. The guild has noticed his work, next week was his 16th and he would be able to apply..."

    "You're ashamed you left it till this late, stuck in disbelief, you will find who did this. Maybe it's not too late after all, even a month later. Maybe he left? Still in your gut you know something insidious is going on.  So you resolve to…"

    default hascard = False

    label choices:

    menu:
        "Check out the Tavern":
            jump choices1_a

        # "Investigate the graveyard":
        #     jump choices1_b

        "Put up posters":
            jump choices1_c


    # nightlife
    label choices1_a:

    scene bg tavern

    "First place to check is the tavern. It's not far from the armory, just down the square. You smell it from here.  Maybe the owner has seen someone shifty. Although he might choose not to tell you anything. Otherwise maybe someone else can be asked, most visit the tavern at least occasionally."
    
    "Arriving at the front, you creak open the door and are greeted with the usual warm atmosphere. This town really doesn’t have any criminal elements, there's no gambling, no fights in the tavern. Rather uncanny really, never experienced anything like it until a few weeks ago. A serving girl rushes by before your attention is redirected."

    bar "“Can I help you captain?”"
    
    "You decide it might be best to try to blend in, lower the guard of those present and observe."

    bar "Captain?"

    "You’ve been staring at the crowd."

    cap "Yes I'll have a drink."

    bar "Very well, a fellow like you deserves something better than the standard swill, I’ll fetch something a little bit fancier."

    "Does he think you're rich? I guess you kind of are, at least compared to most here."

    "You patiently wait, but you aren’t the only one. Patrons are getting restless, how long has it been?"

    menu:
        "Go out back":
            "Something is up, you can't just wait any longer."

            "Patron" "Hey where are you going?"

            "Ignoring them you jump the bar and open the door."

            "Inside is strangely, completely empty. Only a room with an open door and a trail of blood leading inside."

            "You draw your short sword and step in."

            jump choices1_game

        "Wait a bit longer":

            "It must have been at least half an hour, so some of the patrons started banging their cups on the bar."

            "Patron 1" "We want drinks!"

            "Patron 2" "Come on Rob, where are ya!"

            Patron 3 "Calm down lads, the Captain is here."

            "The bartender rushes out."

            bar "Sorry lads, orders?"

            "He seems to have completely forgotten about your order, or was unable to find ‘something fancy’. Wait, is that blood pooling under his shirt?"

            "Just as you are about to speak, you are interrupted."

            bar "Sorry Captain, couldn't find anything! I'll drop a bottle off to the armory later when it comes in."

            "Hmm, well strange but no reason to invade his privacy. You tried listening to the other patrons, but you’ve already been noticed."

            jump choices1_a  

    # graveyard
    label choices1_b:

        "God" "Bad work"
        jump choices1_game

        jump choices

    # posters
    label choices1_c:
        
        "You decide to offer the locals a small reward for information and put up some posters about disappearances. You spend a lot of the day putting these up, in the wide open space of the town square. To the squalad alley of the pub"

        scene bg_town_square_ripped_poster

        "Whilst on patrol for any other spots you pass by the square and notice something."

        "The poster has been removed!"

        show background_actor normal

        "You ask around the stalls for anyone who might have seen something but no one has seen who took the poster."

    menu:
        "Check the other posters":
            jump choices3_a

        "Look at empty stall":
            jump choices3_b

    label choices3_a:

        scene bg pub_ripped_poster
        "You return to the pub which is nearby, but you're too late, it's been ripped off."

        scene bg armory_ripped_poster
        "You check by the front of the militia armory, poster removed! The gall, it's as if they don’t fear being caught as all!"

        "Unfortunately you don’t find anything, time to think of another option."

        jump choices

    label choices3_b:

        scene bg empty_stall

        "The closest stall to the notice board is empty, still with goods on display. Did this stall holder see something? Upon investigation you find a card on the ground…"
        $ hascard = True

        scene bg empty_stall_alley_01

        "Doesn’t give off an encouraging vibe. It was positioned towards a nearby alley. Looks like the back of the tavern. It's worth a look."

        scene bg archery_card_01

        "Did it just blink? You pick it up, the card has a rather rugged looking fellow on it, holding a crossbow. Odd it looks like your archery instructor."

        scene bg archery_card_02

        "You flip it over, the back has some sort of spiral pattern on the back, looking at it almost seems to draw you in."

        "You decide to check out the alley quickly, maybe you can ask around about the card later. You pocket it for now."

        scene bg empty_stall_alley_02

        "The alley appears to go behind the tavern, there's no doors or windows further down is a bend. You turn and…"

        scene bg game_door

        "Find an open door. This must be it, you draw your short sword and enter."

        jump choices1_game

    label choices1_game:

        gm "Welcome a new player"

        cap "Mhmmm!"

        "Your mouth is gagged!"

        gm "The nobles like to play chess but I made a new game, a better one! This isn’t just mere pieces."

        "This guy is mad, there must be a way out?"

        "He shows you a handful of cards. They each have different figures on them, one is an armoured warrior, another is a boar, are they tarot cards?"

        gm "No, these are real! You see I make these cards from actual living beings. Mwahahaha it's quite easy once you have done it, only requires you to catch them." 

        gm "As you can imagine the stakes can thus be quite high, even for me. It's such a hassle to get more cards…"

        "Geez what a madman, it can't be true though right?"

        gm "You’d make a good card I think! But you entered my door, you think to challenge me?"

        "You don’t think he is going to kill you, maybe you can play along, you nod in affirmation."

        gm "So let’s introduce you to the game."

        "He smirks, clearly pleased. Listening to him is painful but you might need to pay attention."

        gm "We each get some cards and we force them to fight each other! These cards have actual souls in them, we may not see the fight, but we will see the outcome. If a card loses a battle it dies, they are gone."

        gm "We can have as many cards as we like, but I doubt you have any. No one does! But I’ll change that soon, I just have to figure out the balance and rules… anyway I’ll give you some of mine, to give you a chance."

        "Figures, even for something new to the game, the odds are stacked in his favor. You are given 3 cards, where he has 4."

        gm "Don't worry about wounded, they heal afterwards. Just look at the cards themselves to see the result!"

        "You look at your cards, they all have a neutral expression. It almost looks like their chest is rising and falling, they also feel… wrong."

        "It's true isn’t it, he is some sort of warlock, you’ve found the missing people."

        gm "Winner is the last one with a card. You lose, get turned into a card!"

        "Surely that’s worse than death."

        "So as you see it, you have to best match up the cards you have against him, in hopes that they will be able to win and hopefully not lose them afterwards."

        gm "Excellent! That’s it. Last rule is the player with the most cards or if they are equal, who loses a coin flip goes first."

        gm "That would be me."

        "What a smug bastard. At least it seems pretty simple, like pitting people against each other in a fight to the death?"

        "You take a look over your cards… there is: A man wearing chainmail armor and wielding a halberd. A woman with a large cooking pot and knife. Lastly, no, this must be a cruel joke."

        "A young man, holding a trap in one hand a short spear in the other. Your son."
        
        gm "Let the game begin!"

        gm "Lets see how you handle this!"

        "The Game Master slams down the first card"

        "It looks like a peasant man, with a bow. Whoever you send here might get shot. But he doesn't have a side arm. So what to play?"

        menu:
            "The halberd wielder.":
                "You stare at the cards expecting something to happen, then you see it. Your card changes."
                "The halberd weilder is dead."
                jump game_round1_23

            "The woman with cookware.":
                "You stare at the cards expecting something to happen, then you see it. The Game Masters card changes."
                "The bow man is dead."
                jump game_round2_123

            "Your son.":
                "You stare at the cards expecting something to happen, then you see it. Your card changes."
                "Your son is dead."
                "You lost your son again, you lose the will to play and forfeit."
                jump game_loss

    label game_round1_23:
        
        menu:
            "The woman with cookware.":
                "You stare at the cards expecting something to happen, then you see it. The Game Masters card changes."
                "The bow man is dead."
                jump game_round2_23

            "Your son.":
                "You stare at the cards expecting something to happen, then you see it. Your card changes."
                "Your son is dead."
                "You lost your son again, you lose the will to play and forfeit."
                jump game_loss

    label game_round2_123:
         gm "What a surprising outcome!"
         
         "It didn't surprise you, the lid of the pot would of made a decent shield. Once in close the peasant man was done for."

         "He places the next card down quite gently."

         gm "Must of been lucky. But just to be sure!"

         "He places the next card down, a man wielding a sword and buckler is next."

         "You know the woman is doomed and are not surprised to see her corpse materialse on the card in front of you."

         gm "I bet it was Easy!"

         menu:
            "The halberd wielder.":
                "You stare at the cards in anticipation, then you see it. The Game Masters card changes."
                "The swordsman is dead."
                jump game_round3_3

            "Your son.":
                "You stare at the cards in anticipation, then you see it. Your card changes."
                "Your son is dead."
                "You lost your son again, you lose the will to play and forfeit."
                jump game_loss

    label game_round2_23:
         gm "What a surprising outcome!"
         
         "It didn't surprise you, the lid of the pot would of made a decent shield. Once in close the peasant man was done for."

         "He places the next card down quite gently."

         gm "Must of been lucky. But just to be sure!"

         "He places the next card down, a man wielding a sword and buckler is next."

         "You know the woman is doomed and are not surprised to see her corpse materialse on the card in front of you."

         gm "I bet it was Easy!"

         menu:     
            "Your son.":
                "You stare at the cards in anticipation, then you see it. Your card changes."
                "Your son is dead."
                "You lost your son again, you lose the will to play and forfeit."
                jump game_loss

    label game_round3_3:

         gm "This one calls for something special I think. Yes!"

         "The next card sends a chill down your spine."

         "It's a knight. Wearing full mail armor and on horseback. Well you never know."

         "The halberd wielder fades away, leaving nothing but another body."

         "Well this is it then, you son has to fight a knight?"

         if hascard:
            "Wait, you almost forgot about it."

            "You reach into your pocket and pull out your hidden card."

            gm "What's this? What a sneaky play!"

            gm "Well it's in the rules!"

            menu:   
                "Your son":
                    "You stare at the cards in anticipation, then you see it. The cards changes."
                    "Both your son and the knight is dead."
                    "You lost your son again, but the game is over."
                    jump game_winloss
                "Archery Instructor":
                    jump game_round4_3


         else:
            "What can you do but pray? He is a strong and clever lad, but what chance has he?"  

            "You stare at the cards in anticipation, then you see it. The cards change."
            "Both your son and the knight is dead."
            "You lost your son again, but the game is over."
            jump game_draw


    label game_round4_3:
        "You stare at the cards in anticipation, then you see it. The cards change."
        "Your instructor is dead. The knight is removed from the card, leaving just his mount."

        gm "Interesting outcome, still a powerful creature though!"

        "You place your sons card down. You watch as the stallion disappears."
        "There is no more cards left but your son, you've won."
        jump game_win
        


    label game_draw:
        gm "A draw! Well done, no one has managed to do that before. You didn’t win but you didn’t lose!"

        gm "Time for your reward, you get to be the new game master!"

        gm "That’s right, now I can focus solely on gathering new pieces! Don’t worry you’ll enjoy it, you’ll get to make your own deck of your choosing, make requests for new ones and challenge other high end players!"

        gm "Isn't this exciting?"

        "He just killed your son and he expects you to help HIM?"

        gm "Now just seal it with an incantation."

        "Well that's it then, you go on to play against lavishly dressed individuals, usually masked. You note flaws in the rules and help the game grow and start to enjoy it."

        "Killing people for this game becomes easy and you forget you were put here forceably. One day you ask the Game Master to join in the 'collections' process."

        "You're better then he could be at the hunts, together you created the grandest collections. You are most proud of your 'children army', always throws your opponents off!"

        # End

        scene bg end_of_game

        "Thanks for playing!"

        return

    label game_win:

        gm "No! You cheated."

        "You shake your head. No, you didn't. He clearly said you can use as much cards as you want."

        gm "I won't! Who will continue my legacy!"

        "All you did was make him angry, is this how it ends? He is now pacing back and forth."

        "Suddenly, he stops and looks at the ground. There's a card there. Within moments you see him DISSOLVE in front of your eyes!"

        "Are you free? Well still need to find a way out of here."

        "Hours later."

        "He got you good, you have to admit. Haven't been able to budge that's for sure and you are starting to get parched. Maybe you think, you might die from thirst."

        "Just then, a door opens from behind."

        bar "Captain?"

        "He walks in and looks around, something catches his eye."

        bar "You done it? His gone!"

        "He picks up a card from the ground and shows it to you."

        "It's him alright. I guess he kept his end of the deal. Or maybe he had no choice."

        "Your not sure what to do now, there is still your son trapped in the card. But the moment your bindings are removed, you almost collapse."

        "You need rest, but hopefully you can reverse the process somehow."

        bar "You look exhausted, come, lay here have a drink or some food if you need. We'll clean this up later."

        scene bg end_of_game

        "Thanks for playing!"

        return

    label game_winloss:

        gm "No! You cheated."

        "You can't be bothered arguing, you've lost your son again. Not that you can say anything anyway."

        gm "I won't! Who will continue my legacy!"

        "Just let it end."

        "Suddenly, he stops and looks at the ground. There's a card there. Within moments you see him DISSOLVE in front of your eyes!"

        "Serves him right."

        "Hours later."

        "He got you good, you have to admit. Haven't been able to budge that's for sure and you are starting to get parched. Maybe you think, you might die from thirst."

        "Just then, a door opens from behind."

        bar "Captain?"

        "He walks in and looks around, something catches his eye."

        bar "You done it? His gone!"

        "He picks up a card from the ground and shows it to you."

        "It's him alright. I guess he kept his end of the deal. Or maybe he had no choice."

        "Your not sure what to do now. But the moment your bindings are removed, you almost collapse."

        "You need rest."

        bar "You look exhausted, come, lay here have a drink or some food if you need. We'll clean this up later."

        scene bg end_of_game

        "Thanks for playing!"

        return

    label game_loss:

        gm "Well that won't do! What a poor challenge."

        gm "Oh well, you seem like a fighter, captain, you’ll be a decent card."

        "This can't be happening!"

        gm "Now stare into this!"

        "You try to look away, but can't by some strange compulsion."

        "It's a card, a blank card."

        "He flips it over, presenting the spiral backing. You start to feel drawed in..."

        "Just like that he clicks he fingers"

        gm "I was wondering where I put that card..."

        "You blacked out for a second, you wake up in a cell."

        "Strangely your wearing your armor, your spear from the armor is nearby as well."

        "Instinctively you grab it, just then the cell door opens"

        "You walk out the entrance, which is a longer corridor going upwards. Looks like you had the only cell in the place."

        "At the end you see a large circular shaped arena."

        "*Bang* A gate slams shut behind you. You see an entrance on the other side, a figure walks out, knife in hand. Your first opponent."

        "You try to resist, but you find yourself compulsed to a ready stance with your spear. Running with your spear out, you break into a charge."

        "As you start to make out the details of your opponent, you plunge your spear in their chest."

        "Impaled and wracked by pain, your wife drops the knife, clearly defeated. You win."

        scene bg end_of_game

        "Thanks for playing!"

        return

    # This ends the game.

    return
